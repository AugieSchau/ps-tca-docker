# Pushing Images to Private Registry

1. Log on to the OpenShift server with SSH
2. Log into OpenShift
   ```
   oc login -u admin -p admin https://localhost:8443
   ```
3. Log into the OpenShift docker registry;
   ```
   docker login -u openshift -p $(oc whoami -t) 172.30.1.1:5000
   ```
4. Mirror the image
   ```
   oc image mirror 172.30.1.1:5000/nycautotrader/nycautotrader:latest dockerreg.conygre.com:5000/nycautotrader/server:latest --insecure=true
   ```

   Replace names accordingly
